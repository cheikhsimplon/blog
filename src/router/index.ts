import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

import AddMarecette from '@/views/AddMarecette.vue'
import LarecetteView from '@/views/LarecetteView.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/Marecette/:id',
      name: 'Larecette',
      component: LarecetteView
    },
    {
      path: '/addMarecette',
      name: 'addMarecette',
      component: AddMarecette

    }


  ]
})

export default router
