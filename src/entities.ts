export interface Marecette {
    contenu:string;
    image?:string;
    title: string;
    Date : string;
    id?:number;

}

export interface Avis {
    id?:number;
    name:string;
    description:string;
    Date : string
}

export interface Categorie {
    name : string;
    Id? : number;
    
}


export interface User {
   
        id?:number;
        email:string;
        password?:string;
        role?:string;
    }
