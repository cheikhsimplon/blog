import type { Marecette } from '@/entities'
import axios from 'axios'

export async function fetchMarecette() {
  const response = await axios.get<Marecette[]>('http://localhost:8080/api/Marecette')
  return response.data
}

export async function fetchOneMarecette(id:any) {
    const response = await axios.get<Marecette>('http://localhost:8080/api/Marecette/'+id);
    return response.data;
    
}

export async function postMarecette(Marecette:Marecette) {
  const response = await axios.post<Marecette>('http://localhost:8080/api/Marecette', Marecette);
  return response.data;
}


export async function deleteMarecette(id:any) {
  await axios.delete<void>('http://localhost:8080/api/Marecette/'+id);
}

export async function updateMarecette(Marecette:Marecette) {
  const response = await axios.put<Marecette>('http://localhost:8080/api/Marecette/'+Marecette.id, Marecette);
  return response.data;
}

